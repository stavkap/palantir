package gr.serresparc.palantir.api;

import gr.serresparc.palantir.dao.UserDAO;
import gr.serresparc.palantir.model.Country;
import gr.serresparc.palantir.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for registering users to a database
 */
@Controller
public class RegistrationController
{
    /**
     * Maps to /registeruser, takes a User object as a parameter and sends the document to a database
     *
     *
     * @return String
     */
    @RequestMapping(value = "/registeruser", method = RequestMethod.POST)
    public String createUser(@RequestParam(value = "name") String firstName,
                             @RequestParam(value = "surname") String lastName,
                             @RequestParam(value = "username") String username,
                             @RequestParam(value = "email") String email,
                             @RequestParam(value = "password") String password
                             )
    {
        User user = new User(password, username, firstName, lastName, email,null,null);
        new UserDAO().createUser(user);

        return "redirect:home";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String userLogin(@RequestParam(value = "username") String username,
                             @RequestParam(value = "password") String password)
    {

        boolean login = new UserDAO().logIn(username, password);
        if(login){
            return "redirect:home";
        }
        else{
            return "redirect:invalidCredentials";
        }
    }
}
