package gr.serresparc.palantir.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * REST controller for the web site
 */
@Controller
public class WebsiteController
{

    /**
     * Maps to /home and returns index.html
     *
     * @return String
     */
    @RequestMapping("/home")
    public String loadHomePage()
    {
        return "index.html";
    }

    /**
     * Maps to /map and returns map/map.html
     *
     * @return String
     */
    @RequestMapping("/map")
    public String loadMap()
    {
        return "map/map.html";
    }

    /**
     * Maps to /register and returns registerform.html
     *
     * @return String
     */
    @RequestMapping("/register")
    public String register() { return "registerform.html";}

    /**
     * Maps to /login and returns login.html
     *
     * @return String
     */
    @RequestMapping("/login")
    public String login() { return "login.html";}


    @RequestMapping("/invalidCredentials")
    public String invalidCredentials() { return "invalidCredentials.html";}

    /**
     * Maps to /chart and returns charts/chart.html
     *
     * @return String
     */
    @RequestMapping("/chart")
    public String showChart() { return "charts/chart.html";}

}
