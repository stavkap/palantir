package gr.serresparc.palantir.api;

import gr.serresparc.palantir.dao.ConflictDAO;
import gr.serresparc.palantir.model.Conflict;
import gr.serresparc.palantir.model.Country;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * REST Controller for mapping Endpoints for the Palantir Conflict Service
 */
@RestController
public class ConflictController
{
    private final ConflictDAO conflictDAO;

    /**
     * Class constructor that initializes a ConflictDAO object
     */
    public ConflictController()
    {
        conflictDAO = new ConflictDAO();
    }

    /**
     * Mapps to /Conflicts and returns a list of conflicts from MongoDB database
     *
     * @return List<Conflict>
     */
    @GetMapping(value = "/Conflicts")
    public List<Conflict> getCoordinates()
    {
        return conflictDAO.readConflicts();
    }

    /**
     * Maps to /Conflicts/perCountry and returns conflicts per country
     *
     * @param countries List<String>
     * @return List<Country>
     */
    @RequestMapping(value = "/Conflicts/perCountry")
    public List<Country> getConflictCounts(@RequestParam("countries") List<String> countries)
    {
        return conflictDAO.conflictsPerCountry(countries);
    }

    /**
     * Maps to /Conflicts/conflictsOnAllCountries and returns conflicts per country
     *
     * @return List<Country>
     */
    @GetMapping(value = "/Conflicts/conflictsOnAllCountries")
    public List<Country> getConflictCounts()
    {
        return conflictDAO.conflictsPerCountry();
    }
}
