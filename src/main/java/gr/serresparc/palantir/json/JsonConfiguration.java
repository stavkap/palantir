package gr.serresparc.palantir.json;

import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


@Configuration
public class JsonConfiguration
{

    @Primary
    @Bean
    public Gson gson()
    {
        return new Gson();
    }
}
