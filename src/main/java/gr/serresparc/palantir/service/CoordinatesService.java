package gr.serresparc.palantir.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Class for coordinate resolution
 */
@Component
public class CoordinatesService
{
    private static final Logger logger = Logger.getLogger(CoordinatesService.class.getName());

    @Value("${GoogleApiKey}")
    private String API_KEY;

    /**
     * Method for resolving coordinates of a location using Google Maps API Geocode
     *
     * @param location String
     * @return LatLng
     */
    public LatLng getLatLng(String location)
    {
        GeoApiContext geoApiContext = new GeoApiContext.Builder().apiKey(API_KEY).build();
        try
        {
            GeocodingResult[] results = GeocodingApi.geocode(geoApiContext, location).await();
            for (AddressComponent addressComponent : results[0].addressComponents)
            {
                for (AddressComponentType componentType : addressComponent.types)
                {
                    if (componentType == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)
                    {
                        logger.info("Resolved Coordinates for " + location + ": " + results[0].formattedAddress + " " +
                                    "LatLng: " + results[0].geometry.location);
                        return results[0].geometry.location;
                    }
                    else if (componentType == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_2)
                    {
                        logger.info("Resolved Coordinates for " + location + ": " + results[0].formattedAddress + " " +
                                    "LatLng: " + results[0].geometry.location);
                        return results[0].geometry.location;
                    }
                    else if (componentType == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_3)
                    {
                        logger.info("Resolved Coordinates for " + location + ": " + results[0].formattedAddress + " " +
                                    "LatLng: " + results[0].geometry.location);
                        return results[0].geometry.location;
                    }
                    else if (componentType == AddressComponentType.LOCALITY)
                    {
                        logger.info("Resolved Coordinates for " + location + ": " + results[0].formattedAddress + " " +
                                    "LatLng: " + results[0].geometry.location);
                        return results[0].geometry.location;
                    }
                    else if (componentType == AddressComponentType.COUNTRY)
                    {
                        logger.info("Resolved Coordinates for " + location + ": " + results[0].formattedAddress + " " +
                                    "LatLng: " + results[0].geometry.location);
                        return results[0].geometry.location;
                    }
                    else return null;
                }
            }
            return results[0].geometry.location;
        }
        catch (ApiException | InterruptedException | IOException | IndexOutOfBoundsException e)
        {
            logger.severe("Coordinates for " + location + " could not be resolved using Google Geocode! Error: " + e);
            return null;
        }
    }
}
